# Redirect Container
Simple container based on nginx alpine-slim which serves up a permanent (301) redirect to the https version along with HSTS headers.
